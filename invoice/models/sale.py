# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import ValidationError

		
class salee_order_inherit(models.Model):
	_inherit = "sale.order"

	date_livraison = fields.Datetime(string='Date de livraison')

	#print report
	@api.multi
	def print_AC(self): 		     
		return self.env['report'].get_action(self, 'invoice.report_accommandes')
	

		
