# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import  osv
from openerp.tools.translate import _
from odoo import api, fields, models, _
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError
from odoo.tools.float_utils import float_round
from datetime import datetime
import operator as py_operator

	# mapping invoice type to journal type
TYPE2JOURNAL = {
	'out_invoice': 'sale',
	'in_invoice': 'purchase',
	'out_refund': 'sale',
	'in_refund': 'purchase',
}

class account_invoice(models.Model):
	_inherit = 'account.invoice'

	document_parents = fields.Char(string='Document parent')

class stock_picking(models.Model):
	_inherit = 'stock.picking'

	test = fields.Boolean(default=True)


class stock_invoice_onshipping(models.Model):

	_name = "stock.invoice.onshipping"
	_description = "Stock Invoice Onshipping"	
	
	journal= fields.Many2one('account.journal', 'Journal', required=True)
	invoice_date = fields.Date('Date de Facture')
	#account = fields.Many2one('account.account', string='Account')


	@api.multi
	def enregistrer(self):
		stock=self._context.get('active_id')
		stock_picking = self.env['stock.picking'].browse(stock)
		stock_picking.test=False
		if stock_picking.picking_type_id.code =='incoming':
			view_id = self.env.ref('account.invoice_supplier_form').id
			account_partner=stock_picking.partner_id.property_account_payable_id.id
			account_product=self.journal.default_debit_account_id.id
			type_invoice='in_invoice'
		else:
			view_id = self.env.ref('account.invoice_form').id
			account_partner=stock_picking.partner_id.property_account_receivable_id.id
			account_product=self.journal.default_credit_account_id.id
			type_invoice='out_invoice'
								
		invoice= self.env['account.invoice'].create({
												
						'partner_id':stock_picking.partner_id.id,
						'date_invoice':self.invoice_date or stock_picking.min_date ,
						'journal_id':self.journal.id,
						'account_id':account_partner,
						'origin': stock_picking.name,
						'document_parents': stock_picking.origin,
						'type':type_invoice,
						'state'	:'draft'	
						})

		ligne_invoice= self.env['account.invoice.line']
		for obj in stock_picking.pack_operation_product_ids:
			ligne_invoice.create({
				'product_id':obj.product_id.id,
				'name':obj.product_id.name,
				'quantity':obj.qty_done,
				'price_unit':obj.product_id.lst_price,	
				'account_id':account_product,						
				'invoice_id':invoice.id,
				})

		
		context = self._context.copy()
		invoice_id = self.env['account.invoice'].search([('origin', '=', stock_picking.name)])
		return {
			'name':'account.invoice.form',
			'view_type':'form',
			'view_mode':'form,tree',
			'views' : [(view_id,'form')],
			'res_model':'account.invoice',
			'view_id':view_id,
			'type':'ir.actions.act_window',
			'res_id':invoice_id.id,
			'target': 'current',
			'context':context,
		}
